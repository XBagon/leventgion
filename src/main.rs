use legion::prelude::*;

fn main() {
    // Create a world to store our entities
    let universe = Universe::new();
    let mut world = universe.create_world();

// Create entities with `Position` and `Velocity` data
    world.insert(
        (),
        vec![(events::flare::On::new(|_,_| println!("test")),)]
    );

    let mut query = <(Read<events::flare::On>)>::query();
    let events: Vec<_> = query.iter(&mut world).map(|event| (*event).clone()).collect();
    for event in events {
        event.trigger(&mut world);
    }

}


pub mod events {
    pub mod flare {
        use legion::prelude::*;

        #[derive(Clone)]
        pub struct Before {
            trigger: fn(&Self, &mut World)
        }

        impl Before {
            pub fn new(trigger: fn(&Self, &mut World)) -> Self {
                Self {trigger}
            }

            pub fn trigger(&self, mut world: &mut World) {
                (self.trigger)(&self, &mut world);
                let mut query = <(Read<On>)>::query();
                let events: Vec<_> = query.iter(&mut world).map(|event| (*event).clone()).collect();
                for event in events {
                    event.trigger(&mut world);
                }
            }
        }

        #[derive(Clone)]
        pub struct On {
            trigger: fn(&Self, &mut World)
        }

        impl On {
            pub fn new(trigger: fn(&Self, &mut World)) -> Self {
                Self {trigger}
            }

            pub fn trigger(&self, mut world: &mut World) {
                (self.trigger)(&self, &mut world);
                let mut query = <(Read<After>)>::query();
                let events: Vec<_> = query.iter(&mut world).map(|event| (*event).clone()).collect();
                for event in events {
                    event.trigger(&mut world);
                }
            }
        }

        #[derive(Clone)]
        pub struct After {
            trigger: fn(&Self, &mut World)
        }

        impl After {
            pub fn new(trigger: fn(&Self, &mut World)) -> Self {
                Self {trigger}
            }

            pub fn trigger(&self, mut world: &mut World) {
                (self.trigger)(&self, &mut world);
            }
        }
    }

}
